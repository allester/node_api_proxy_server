const express = require('express')
const cors = require('cors')
const rateLimit = require('express-rate-limit')
const dotenv = require('dotenv')

dotenv.config()

const port = process.env.PORT || 5001

const app = express()

// Rate Limiting
const limiter = rateLimit({
    windowMs: 10 * 60 * 1000, // 10 minutes
    max: 100
})
app.use(limiter)
app.set('trust proxy', 1)

// Set static folder
app.use(express.static('public'))

// Routes
app.use('/api', require('./routes/index'))

// Enable cors
app.use(cors())

app.listen(port, () => {
    console.log(`Server running on port ${port}`)
})